<div  align="center"><h1>TITRE CONCEPTEUR DEVELOPPEUR D'APPLICATIONS<br>RNCP 31678 (code titre TP-01281)</h1></div>
<div  align="center"><h2>EPSI RENNES B3 CDA IA 2022/2023</h2></div>
<div  align="center"><h2>https://www.francecompetences.fr/recherche/rncp/31678/</h2></div>

## Compétences attendues

| COMPETENCE PROFESSIONNELLE                                                                                                                            | PROJET                       |
|-------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------|
| 1  - Maquetter une application                                                                                                                        | mermaid :^)                  |
| 2  - Développer une interface utilisateur de type desktop                                                                                             | projet cda cyber/ai          |
| 3  - Développer des composants d’accès aux données                                                                                                    | Marshmallow/SQLalchemy       |
| 4  - Développer la partie front-end d’une interface utilisateur web                                                                                   | vue + template flask         |
| 5  - Développer la partie back-end d’une interface utilisateur web                                                                                    | flask                        |
| 6  - Concevoir une base de données                                                                                                                    | Postgres/Mongo/Elasticsearch |
| 7  - Mettre en place une base de données                                                                                                              | dbuser.sql                   |
| 8  - Développer des composants dans le langage d’une base de données                                                                                  | procedure stockées/view      |
| 9  - Collaborer à la gestion d’un projet informatique <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Et à l’organisation de l’environnement de développement | git                          |
| 10 - Concevoir une application                                                                                                                        | projet cda cyber/ai          |
| 11 - Développer des composants métier                                                                                                                 | YaraSuricata/Scikit          |
| 12 - Construire une application organisée en couches                                                                                                  | projet cda cyber/ai          |
| 13 - Développer une application mobile                                                                                                                | flutter/KivyBeeWare/Golang   |
| 14 - Préparer et exécuter les plans de tests d’une application                                                                                        | Pytest/CI-CD                 |
| 15 - Préparer et exécuter le déploiement d’une application                                                                                            | Docker/Ansible               |

## Projet d'entreprise

### Activités

- AI (projet de l'année)


- Flask Docker Postgres SQLAlchemy Marshmallow
- Watcher (ECW)
- Doc Yara Suricata
- Manuel utilisateur (charger un jeu de règle suricata depuis un deployer)

### Outils

- Pycharm
- Gitlab
- Python Flask Pytest ScikitLearning 
- Docker
- Postgres

## Projet CDA

### Maquette

```mermaid
flowchart 
    subgraph CDA
        direction LR
        subgraph front
            cli
            web
            mobile
        end
        subgraph back
            subgraph Database
            direction LR
                db_user[(user)]
                db_data[(data)]
                db_log[(log)]
            end
            subgraph api
                direction TB
                api_data
                api_user
                api_log
            end
            subgraph app
            direction LR
                subgraph AI
                    matplotlib
                    models
                end
                subgraph Cyber
                    Watcher
                    YaraSuricata
                end
            end
        end
    end
```

### TODO

- IAM KeyCloak & Role
- Rust
- CTF
- gibson
- gibson
- gibson